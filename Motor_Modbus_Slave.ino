// Author: Leo Bach
// Email: bach_leo@whirli.io
// Wiring Instructions: https://gitlab.com/machinesaver/modbus_motor_controller
// Hardware: Arduino MKR 1010 + MKR485 Shield + CL57T + 17HS13-0404D-E1000

#include <ArduinoRS485.h> // ArduinoModbus depends on the ArduinoRS485 library
#include <ArduinoModbus.h>
#include <stdio.h>
#include <stdint.h>

//#include <EEPROM.h> // For persistent memory between poweroffs (Ex. position or serial number)

// Arduino Modbus Variables
int slave_id = 5;
long baudrate = 115200;

// Motor 1 Pin Settings
int m1_dir_plus = 1;
int m1_pul_plus = 2;
int pulse_delay_us = 200; //200 microseconds minimum - manufacturer


// Initial Register Settings
float m1_target_mils = 0.0;
float m1_pos_mils = 0.0;


const float mils_per_revolution = 25.0;  // Determined from the Micrometer
const float steps_per_revolution = 5000.0; // Stepper Driver Dipswitch Settings
const float steps_per_mil= steps_per_revolution/mils_per_revolution;
const float mils_per_step = 1/steps_per_mil;
//float m1_position = (m1_steps + m1_target)*mils_per_step;
//const float m1_pos_max = 600.0;
//const float m1_pos_min = 0.0;


//Functions to extract the HighWord(16bits) and LowWord(16-bits)from a long(32bit)
#define highWord(w) ((w) >> 16)
#define lowWord(w) ((w) & 0xffff)

uint16_t MSB_16bit_of_float32 ( float float_number){
  union
  {
    float f_number;
    uint16_t uint16_arr[2];
  } union_for_conv;  
  union_for_conv.f_number = float_number;
  uint16_t MSB_uint16 = union_for_conv.uint16_arr[1];
  return MSB_uint16;
}

uint16_t LSB_16bit_of_float32 (float float_number){
  union
  {
    float f_number;
    uint16_t uint16_arr[2];
  } union_for_conv;  
  union_for_conv.f_number = float_number;
  uint16_t LSB_uint16 = union_for_conv.uint16_arr[0];
  return LSB_uint16;
}

float float32_from_two_uint16(uint16_t MSB_uint, uint16_t LSB_uint){
  union
  {
    float f_number;
    uint16_t uint16_arr[2];
  } union_for_conv;  
  union_for_conv.uint16_arr[0] = LSB_uint;
  union_for_conv.uint16_arr[1] = MSB_uint;
  return union_for_conv.f_number;
}

//int main ()
//{
//  float a;
//  uint16_t b;
//  uint16_t c;
//  float d;
//  float no_lsb_float;
//
//  a = -693.42056;
//  b = MSB_16bit_of_float32(a);
//  c = LSB_16bit_of_float32(a);
//  d = float32_from_two_uint16(b,c);
//  no_lsb_float = float32_from_two_uint16(b,0);
//  printf ("original f: %f\t MSB16: %d\tLSB16:%d\t final f:%f\t no_lsb_float f:%f\n", a, b,c, d,no_lsb_float);
//
//}

void setup() {
  // set pins on arduino nano for driving motor #1 
  pinMode(m1_dir_plus,OUTPUT); //A DIGITAL PIN ---> DIR+
  pinMode(m1_pul_plus,OUTPUT); //A PWM PIN ---> PUL+
  // set pins on arduino nano for driving motor #2
  //  pinMode(mtr_2_dir_plus,OUTPUT); //A DIGITAL PIN ---> DIR+
  //  pinMode(mtr_2_pul_plus,OUTPUT); //A PWM PIN ---> PUL+
  // DIR- & PUL- should be connected to the GND pin of the Arduino Nano

  // start the Modbus RTU server, with (slave) id 5
  if (!ModbusRTUServer.begin(slave_id, baudrate)) {
    Serial.println("Failed to start Modbus RTU Server!");
    while (1);
  }
  // configure holding registers addresses starting at 0x00, how many to setup
  ModbusRTUServer.configureHoldingRegisters(0x00, 124);
  
  // Initial Conditions
  ModbusRTUServer.holdingRegisterWrite(0x00, MSB_16bit_of_float32(m1_target_mils)); // motor 1 target
  ModbusRTUServer.holdingRegisterWrite(0x01, LSB_16bit_of_float32(m1_target_mils));
  ModbusRTUServer.holdingRegisterWrite(0x02, MSB_16bit_of_float32(m1_pos_mils)); // motor 1 position
  ModbusRTUServer.holdingRegisterWrite(0x03, LSB_16bit_of_float32(m1_pos_mils));
  }

void loop() {
  // poll for Modbus RTU requests
  ModbusRTUServer.poll();
  //  update target incase user wrote to it
  m1_target_mils = float32_from_two_uint16(ModbusRTUServer.holdingRegisterRead(0x00),ModbusRTUServer.holdingRegisterRead(0x01));
  if(m1_target_mils > m1_pos_mils){
      digitalWrite(m1_dir_plus,HIGH); // Set Move Direction Away From Probe
      while(m1_pos_mils < m1_target_mils){
        digitalWrite(m1_pul_plus,HIGH);
        delayMicroseconds(pulse_delay_us);
        digitalWrite(m1_pul_plus,LOW);
        delayMicroseconds(pulse_delay_us);
        m1_pos_mils = m1_pos_mils + (1.0*mils_per_step);
      }
      ModbusRTUServer.holdingRegisterWrite(0x02,MSB_16bit_of_float32(m1_pos_mils));
      ModbusRTUServer.holdingRegisterWrite(0x03,LSB_16bit_of_float32(m1_pos_mils));
  }
  if(m1_target_mils < m1_pos_mils){
    digitalWrite(m1_dir_plus,LOW); // Set Move Direction Away From Probe
    while(m1_pos_mils > m1_target_mils){
      digitalWrite(m1_pul_plus,HIGH);
      delayMicroseconds(pulse_delay_us);
      digitalWrite(m1_pul_plus,LOW);
      delayMicroseconds(pulse_delay_us);
      m1_pos_mils = m1_pos_mils - (1.0*mils_per_step);
    }
    ModbusRTUServer.holdingRegisterWrite(0x02,MSB_16bit_of_float32(m1_pos_mils));
    ModbusRTUServer.holdingRegisterWrite(0x03,LSB_16bit_of_float32(m1_pos_mils));
  }
}
