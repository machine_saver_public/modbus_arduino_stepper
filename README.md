# modbus_arduino_stepper

An Arduino Modbus slave for interacting with up to two closed loop stepper motor drivers.

# hardware

1. MKR1010 Wifi

2. MKR485 RS485 Shield

3. Closed Loop Stepper Motor Driver (CL57T)

4. Stepper Motor + Encoder (17HS13-0404D-E1000)

5. RS485 USB